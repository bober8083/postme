import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Postme {
    JFrame frame = new JFrame("FlowLayout demo");
    JPanel panel1 = new JPanel();
    JPanel panel2 = new JPanel();
    JButton btn1 = new JButton("First");
    JButton btn2 = new JButton("Second");
    JButton btn3 = new JButton("Third");

    JTextField textfield = new JTextField("Hello from JTextField", 10);

    public Postman2() {
        panel1.setLayout(new FlowLayout(FlowLayout.RIGHT, 3, 3));
        panel1.add(btn1);
        panel1.add(btn2);
        panel1.add(btn3);

        frame.add(panel1);

        panel2.add(textfield);
        frame.add(panel2);

        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        /*SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Postman2();
            }
        });*/

        ping();
        test();

        //add params
        //https://zetcode.com/java/getpostrequest/
    }

    public static void ping() {
        try {
            pingInternal();
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static void test() {
        try {
            postTestInternal();
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    private static void pingInternal() throws IOException {
        URL url = new URL("http://localhost:8082/ping");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        StringBuilder content;

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String line;
            content = new StringBuilder();
            while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        }

        System.out.println(content);
        con.disconnect();
    }

    private static void postTestInternal() throws IOException {
        URL url = new URL("http://localhost:8082/test");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setRequestProperty("User-Agent", "Java client");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        String urlParameters = "name=Jack&occupation=programmer";
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.write(postData);
        }

        StringBuilder content;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String line;
            content = new StringBuilder();
            while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        }

        System.out.println(content);
        con.disconnect();
    }
}
